function [] = mainAssignment2()
%function [] = mainAssignment2(deck1Play,deck2Play,crossfadeL,crossfadeR)
    %test
    disp("Launching Main Function ...");
% close all;
% clear all;
% clc;

    figure;
    animationSpeed = 0.02;
    
    %%SETUP

    % assign cute base locations
    cuteBase = [0,0,0]; % cute base location
    
    % plot environment
    plotEnvironment([0.025,0.2,0.05]);

    % asssign part locations
    deck1PlayLoc = [0.025,0.2,0.05]+[-0.15,-0.1,0.1];
    deck2PlayLoc = [0.025,0.2,0.05]+[0.15,-0.1,0.1];
    crossfadeLLoc = [0.025,0.2,0.05]+[-0.03,-0.1,0.1];
    crossfadeRLoc = [0.025,0.2,0.05]+[0.03,-0.1,0.1];

    deck1Play = transl(deck1PlayLoc)*trotx(deg2rad(180)); % deck 1 play button location
    deck2Play = transl(deck2PlayLoc)*trotx(deg2rad(180)); % part 2 location
    crossfadeL = transl(crossfadeLLoc)*trotx(deg2rad(180)); % part 3 location
    crossFadeM = transl(((crossfadeLLoc+crossfadeRLoc)/2))*trotx(deg2rad(180)); % part 3 location
    crossfadeR = transl(crossfadeRLoc)*trotx(deg2rad(180)); % drop off location


    %% create cute robot
    disp("Assembling Robot ...");
    cuteRobot = Cute;
    q0 = zeros(1,7); %initial joint angles

    % set cute base location
    disp(transl(cuteBase));
    cuteRobot.base = cuteBase;

    % Plot the robot
    hold on;
    cuteRobot.GetCuteRobot();
    cuteRobot.PlotAndColourRobot();
    teach(cuteRobot.model);

    %% Forward kinematics to get to key location joint angles

    % left ur3
    d1PlayQ = cuteRobot.model.ikcon(deck1Play);
    d2PlayQ = cuteRobot.model.ikcon(deck2Play);
    crossLQ = cuteRobot.model.ikcon(crossfadeL);
    crossMQ = cuteRobot.model.ikcon(crossFadeM);
    crossRQ = cuteRobot.model.ikcon(crossfadeR);

    %% Trajectories betwen locations

    steps = 50;

    initToD1Play = jtraj(q0,d1PlayQ,steps);             % origin to deck 1 play button
    d1PlayToRest = jtraj(d1PlayQ,q0,steps);             % deck 1 play button to rest
    restToD2Play = jtraj(q0,d2PlayQ,steps);             % rest to deck 2 play button
    d2playToRest = jtraj(d2PlayQ,q0,steps);             % deck 2 play button to rest
    restToCrossL = jtraj(q0,crossLQ,steps);             % rest to cross fader left
    crossLToCrossM = jtraj(crossLQ,crossMQ,steps);      % cross fader left to cross fader right
    crossMToCrossR = jtraj(crossMQ,crossRQ,steps);      % cross fader left to cross fader 
    crossRToRest = jtraj(crossRQ,q0,steps);             % cross fader left to cross fader right

    %% Move Arms to Required Locations
    C = load('cyton_q.mat');
    
%     disp("--- DO THE THING ---");
%     disp(crossfadeL);
%     for i = 1:787
%         animate(cuteRobot.model,C.cyton_q(i,1:7));
%         drawnow;
%         pause(animationSpeed);
%     end
    
    
    disp("--- Press PLAY Deck 1 ---");
    disp(deck1PlayLoc);
    for i = 1:1:steps
        animate(cuteRobot.model,initToD1Play(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
    disp("--- Move to Rest Pos ---");
    for i = 1:1:steps
        animate(cuteRobot.model,d1PlayToRest(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
    disp("--- Press PLAY Deck 2 ---");
    disp(deck2PlayLoc);
    for i = 1:1:steps
        animate(cuteRobot.model,restToD2Play(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
    disp("--- Move to Rest Pos ---");
    disp(crossfadeL);
    for i = 1:1:steps
        animate(cuteRobot.model,d2playToRest(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
    disp("--- Move to Cross Fader Left ---");
    disp(crossfadeL);
    for i = 1:1:steps
        animate(cuteRobot.model,restToCrossL(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
        disp("--- Move to Cross Fader Mid ---");
    for i = 1:1:steps
        animate(cuteRobot.model,crossLToCrossM(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
    disp("--- Move to Cross Fader Right ---");
    for i = 1:1:steps
        animate(cuteRobot.model,crossMToCrossR(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
    disp("--- Move to Cross Fader Right ---");
    for i = 1:1:steps
        animate(cuteRobot.model,crossRToRest(i,1:7));
        drawnow;
        pause(animationSpeed);
    end
    
    disp("--- Complete - Drop the Bass! ---");
end

