function [] = plotEnvironment(pos)

%DDJ WeGo
axis manual
environment = transl(pos);
[f2,v2,data2] = plyread('wego.ply','tri');

% Get vertex count
object2VertexCount = size(v2,1);

% % Move center point to origin
midPoint = sum(v2)/object2VertexCount;
object2Verts = v2 - repmat(midPoint,object2VertexCount,1);

% Create a transform to describe the location (at the origin, since it's centered
object2Pose = environment;

% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [data2.vertex.red, data2.vertex.green, data2.vertex.blue] / 255;

% Then plot the trisurf
object2Mesh = trisurf(f2,object2Verts(:,1),object2Verts(:,2), object2Verts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints2 = [object2Pose * [object2Verts,ones(object2VertexCount,1)]']'; 
object2Mesh.Vertices = updatedPoints2(:,1:3);
end

